#ifndef SUBJECTWIDGET_H
#define SUBJECTWIDGET_H

#include <QWidget>
#include <QJsonArray>
#include <QJsonObject>

namespace Ui {
class SubjectWidget;
}

class SubjectWidget : public QWidget
{
    Q_OBJECT

public:
    explicit SubjectWidget(QWidget *parent = 0);
    ~SubjectWidget();
    void addButton(QString text,QString id);
    void clearButtons();
    void populate(QJsonObject dataObj);

private slots:
    void subjectButtonClicked();

    void on_pushButton_clicked();

private:
    Ui::SubjectWidget *ui;

signals:
    void sendClose();
    void sendSubject(QString subjectId);
};

#endif // SUBJECTWIDGET_H
