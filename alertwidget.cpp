#include "alertwidget.h"
#include "ui_alertwidget.h"

AlertWidget::AlertWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::AlertWidget)
{
    ui->setupUi(this);

}


void AlertWidget::addButton(QString text,QString id){

    QPushButton* button = new QPushButton();
    connect(button,SIGNAL(clicked(bool)),this,SLOT(alertButtonClicked()));
    button->setText(text);

    button->setProperty("id",id);

    button->setMinimumHeight(65);
    button->setMaximumHeight(65);
    button->setMinimumWidth(600);
    button->setMaximumWidth(600);
    button->setSizePolicy(QSizePolicy::Minimum,QSizePolicy::Maximum);
    button->setFocusPolicy(Qt::NoFocus);
    ui->buttonArea->layout()->addWidget(button);

    unsigned int expectedHeight = 105 + ui->buttonArea->layout()->count()*65;

    if(expectedHeight > 350)
        resize(width(),expectedHeight);

}

void AlertWidget::clearButtons(){

    QLayoutItem *child;
    while ((child = ui->buttonArea->layout()->takeAt(0)) != 0) {
       delete child->widget();
    }
    resize(width(),350);
}

void AlertWidget::populate(QJsonObject dataObj){
    QJsonArray jsonArray;
    clearButtons();
    jsonArray = dataObj["alertList"].toArray();
    for(int i = 0; i < jsonArray.size(); i++){
        QJsonObject obj = jsonArray.at(i).toObject();
        addButton(obj["text"].toString(),obj["id"].toString());
    }
}

void  AlertWidget::alertButtonClicked(){
    emit sendAlert(QObject::sender()->property("id").toString());
}

AlertWidget::~AlertWidget()
{
    delete ui;
}

void AlertWidget::on_pushButton_clicked()
{
    emit sendClose();
}
