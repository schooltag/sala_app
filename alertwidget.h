#ifndef ALERTWIDGET_H
#define ALERTWIDGET_H

#include <QWidget>
#include <QJsonObject>
#include <QJsonArray>

namespace Ui {
class AlertWidget;
}

class AlertWidget : public QWidget
{
    Q_OBJECT

public:
    explicit AlertWidget(QWidget *parent = 0);
    ~AlertWidget();
    void addButton(QString text,QString id);
    void clearButtons();
    void populate(QJsonObject dataObj);
private slots:
    void alertButtonClicked();
    void on_pushButton_clicked();

signals:
    void sendClose();
    void sendAlert(QString alert);

private:
    Ui::AlertWidget *ui;
};

#endif // ALERTWIDGET_H
