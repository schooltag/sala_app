#ifndef INFOWIDGET_H
#define INFOWIDGET_H

#include <QWidget>
#include <QJsonObject>
#include <QTimer>
#include <QDesktopWidget>

namespace Ui {
class InfoWidget;
}

class InfoWidget : public QWidget
{
    Q_OBJECT

public:
    explicit InfoWidget(QWidget *parent = 0);
    ~InfoWidget();
    void updateSize();
    void populate(QJsonObject jsonObjData);


signals:
    void sendClose();

private slots:
    void on_overlay_button_clicked();

private:
    Ui::InfoWidget *ui;
};

#endif // INFOWIDGET_H
