#ifndef PINWIDGET_H
#define PINWIDGET_H

#include <QWidget>
#define MAXCHAR 4

namespace Ui {
class PINWidget;
}

class PINWidget : public QWidget
{
    Q_OBJECT

public:
    explicit PINWidget(QWidget *parent = 0);
    void clearPIN();
    ~PINWidget();

signals:
    sendClose();
    sendPin(QString pin);

private slots:
    void on_pushButton_clicked();
    void on_pushButton_2_clicked();
    void on_pushButton_3_clicked();
    void on_pushButton_4_clicked();
    void on_pushButton_5_clicked();
    void on_pushButton_6_clicked();
    void on_pushButton_7_clicked();
    void on_pushButton_8_clicked();
    void on_pushButton_9_clicked();
    void on_pushButton_10_clicked();
    void on_pushButton_del_clicked();

    void on_pushButton_12_clicked();

    void on_pushButton_11_clicked();

private:
    Ui::PINWidget *ui;
    QString pin;
};

#endif // PINWIDGET_H
