#include "subjectwidget.h"
#include "ui_subjectwidget.h"

SubjectWidget::SubjectWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SubjectWidget)
{
    ui->setupUi(this);
}

SubjectWidget::~SubjectWidget()
{
    delete ui;
}

void SubjectWidget::addButton(QString text,QString id){
    QPushButton* button = new QPushButton();
    connect(button,SIGNAL(clicked(bool)),this,SLOT(subjectButtonClicked()));
    button->setText(text);

    button->setProperty("id",id);

    button->setMinimumHeight(65);
    button->setMaximumHeight(65);
    button->setMinimumWidth(450);
    button->setMaximumWidth(450);
    button->setSizePolicy(QSizePolicy::Minimum,QSizePolicy::Maximum);
    button->setFocusPolicy(Qt::NoFocus);

    unsigned int widgetCount = ui->butonAreaLayout->count();

    ui->butonAreaLayout->addWidget(button,widgetCount/2,widgetCount%2);
}

void SubjectWidget::clearButtons(){
    QLayoutItem *child;
    while ((child = ui->butonAreaLayout->takeAt(0)) != 0) {
       delete child->widget();
    }
}

void SubjectWidget::populate(QJsonObject dataObj){
    QJsonArray jsonArray;
    clearButtons();
    jsonArray = dataObj["subjectList"].toArray();
    for(int i = 0; i < jsonArray.size(); i++){
        QJsonObject obj = jsonArray.at(i).toObject();
        addButton(obj["name"].toString(),obj["id"].toString());
    }
}

void SubjectWidget::subjectButtonClicked(){
    QString id = QObject::sender()->property("id").toString();
    emit sendSubject(id);
}

void SubjectWidget::on_pushButton_clicked()
{
    emit sendClose();
}
