#include "loginwidget.h"
#include "ui_loginwidget.h"

LoginWidget::LoginWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::LoginWidget)
{
    ui->setupUi(this);
}

LoginWidget::~LoginWidget()
{
    delete ui;
}


void LoginWidget::populate(QJsonObject jsonObj){
    QString fullname;
    QStringList roleList,roleListId;
    dataArray = jsonObj["userList"].toArray();
    ui->comboBox->blockSignals(true);
    ui->comboBox->clear();
    ui->comboBox_2->clear();



    for(int i = 0; i < dataArray.size(); i++){
        QJsonObject obj = dataArray.at(i).toObject();
        QString roleString = obj["role_name"].toString();
        if(!roleList.contains(roleString)){
            roleList.append(roleString);
            ui->comboBox->addItem(roleString,obj["role_id"].toString());
        }
    }

    if(ui->comboBox->count() == 0){
        return;
    }

    ui->comboBox->setCurrentIndex(0);
    on_comboBox_currentIndexChanged(ui->comboBox->currentText());

    ui->comboBox->blockSignals(false);
}

void LoginWidget::on_comboBox_currentIndexChanged(const QString &roleText)
{
    ui->comboBox_2->clear();
    for(int i = 0; i < dataArray.size(); i++){
        QJsonObject obj = dataArray.at(i).toObject();
        QString roleString = obj["role_name"].toString();
        if(roleString == roleText){
            QString fullname = obj["user_name"].toString() + ' ' + obj["user_last_name"].toString();
            ui->comboBox_2->addItem(fullname,obj["user_id"].toString());
        }
    }

    ui->comboBox_2->setCurrentIndex(0);
}



void LoginWidget::on_pushButton_clicked()
{
    emit showPIN();
}

void LoginWidget::setLogin(QString pin){
   emit sendLogin(ui->comboBox_2->currentText(),ui->comboBox_2->currentData().toString(),ui->comboBox->currentData().toString(),pin);
}

void LoginWidget::updateSize(QSize desktopSize){
    resize(desktopSize);
    ui->frame->resize(desktopSize);
}



