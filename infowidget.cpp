#include "infowidget.h"
#include "ui_infowidget.h"

InfoWidget::InfoWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::InfoWidget)
{
    ui->setupUi(this);

}

InfoWidget::~InfoWidget()
{
    delete ui;
}

void InfoWidget::populate(QJsonObject jsonObjData){
    QString text = jsonObjData["text"].toString();
    QString title = jsonObjData["title"].toString();
    QString level = jsonObjData["level"].toString();
    if(level == "4") ui->symbol->setProperty("cross",true);
    else ui->symbol->setProperty("cross",false);
    setStyleSheet(styleSheet());

    ui->label_title->setText(title);
    ui->label_text->setText(text);

}



void InfoWidget::updateSize(){
    int width = QDesktopWidget().width();
    int height = QDesktopWidget().height();

    resize(width,height);
    ui->frame->resize(width,height);
    ui->overlay_button->resize(width,height);
}

void InfoWidget::on_overlay_button_clicked()
{
    emit sendClose();
}
