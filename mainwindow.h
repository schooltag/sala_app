#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTcpSocket>
#include <QHostAddress>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QTimer>
#include <QMovie>
#include <QDesktopWidget>
#include <QNetworkInterface>
#include "loginwidget.h"
#include "gradeswidget.h"
#include "classwidget.h"
#include "infowidget.h"
#include "alertwidget.h"
#include "pinwidget.h"
#include "subjectwidget.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
    void readyRead();
    void connected();
    void disconnected();
    void login(QString userName,QString userId,QString teacherName,QString roleId);
    void grade(QString gradeId,QString className);
    void confirmClass(QJsonArray array);
    void closeGrades();
    void closeClass();
    void timerInitTimeout();
    void receptionTimerTimeout();
    void updateStudentStauts(QString studentId,bool isPresent,QString text,QString id);
    void sendSubject(QString subjectId);
    void asist();
    void sendAsist(QString id);


private slots:
    void on_refreshButton_clicked();
    void hideOverlay();
    void confirmInfo();
    void hideAlert();
    void hideInfo();
    void hidePIN();
    void hidePINBlock();
    void hideSubject();
    void showOverlay();
    void showInfo();
    void showAlert();
    void showPIN();
    void showPINBlock();
    void showSubject();
    void logout();
    void logback(QString);

private:
    void resizeEvent(QResizeEvent *event);
    Ui::MainWindow *ui;
    LoginWidget* loginWidget;
    GradesWidget* gradesWigdet;
    ClassWidget* classWidget;
    InfoWidget* infoWidget;
    AlertWidget* alertWidget;
    PINWidget* pinWidget,*pinBlockWidget;
    SubjectWidget* subjectWidget;


    QWidget* currentScreen;
    QTcpSocket* socket;
    QString userName,className;
    QString deviceId;
    QString userId;
    QString roleId;
    QString gradeId;
    QString subjectId;
    QString messageId;
    QString pin;
    void setStyleSheets();
    QTimer* timerInit,* receptionTimer;
    void tryToConnect();
    QMovie* loadingGif;
    QByteArray receptionBuffer;
    bool isOverlayShown;
    bool isInfoShown,isAlertShown,isPinShown,isPinBlockShown,isSubjectShown;
};

#endif // MAINWINDOW_H
