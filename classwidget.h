#ifndef CLASSWIDGET_H
#define CLASSWIDGET_H

#include <QWidget>
#include <QJsonArray>
#include <QJsonObject>
#include <QList>
#include <QPushButton>
#include <QDesktopWidget>
#include <QTimer>


namespace Ui {
class ClassWidget;
}

class ClassWidget : public QWidget
{
    Q_OBJECT

public:
    explicit ClassWidget(QWidget *parent = 0);
    ~ClassWidget();
    void setReasonsPresent(QJsonArray jsonArray);
    void setReasonsAbsent(QJsonArray jsonArray);
    void populate(QJsonObject jsonObjData);
    void updateStudent(QJsonObject jsonObjData);
    void setTeacherLabel(QString teacherLabel);
    void updateStyleSheet(QString stylesheet);
    void updateSize(QSize desktopSize);

private slots:
    void on_back_clicked();
    void on_confirmationButton_clicked();
    void student_pushbutton_pressed();
    void timeout();
    void on_asistButton_clicked();

    void on_blockButton_clicked();

public slots:

signals:
    void confirmClass(QJsonArray array);
    void sendClose();
    void asist();
    void block();

private:
    Ui::ClassWidget *ui;
    QList <QPushButton*> student_pushbutton_list;
    QTimer* timer;
};




#endif // CLASSWIDGET_H
