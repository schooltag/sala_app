#ifndef LOGINWIDGET_H
#define LOGINWIDGET_H

#include <QWidget>
#include <QJsonArray>
#include <QJsonObject>
#include <QDesktopWidget>

namespace Ui {
class LoginWidget;
}

class LoginWidget : public QWidget
{
    Q_OBJECT

public:
    explicit LoginWidget(QWidget *parent = 0);
    ~LoginWidget();
    void populate(QJsonObject jsonObj);
    void updateSize(QSize desktopSize);
    void updateUsers(QJsonObject jsonObj);

signals:
    void sendLogin(QString userName,QString userId,QString roleId,QString pin);
    void sendRole(QString roleId);
    void showPIN();


private slots:
    void on_pushButton_clicked();
    void setLogin(QString pin);

    void on_comboBox_currentIndexChanged(const QString &arg1);

private:
    Ui::LoginWidget *ui;
    QJsonArray dataArray;
};

#endif // LOGINWIDGET_H
