#ifndef GRADESWIDGET_H
#define GRADESWIDGET_H

#include <QWidget>
#include <QList>
#include <QPushButton>
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonDocument>
#include <QDesktopWidget>

namespace Ui {
class GradesWidget;
}

class GradesWidget : public QWidget
{
    Q_OBJECT

public:
    explicit GradesWidget(QWidget *parent = 0);
    ~GradesWidget();
    void populate(QJsonObject jsonObj);
    void unselect();
    void setTeacherLabel(QString teacherLabel);
    void updateSize(QSize desktopSize);

signals:
    void sendGrade(QString gradeId,QString gradeName);
    void sendClose();

private slots:
    void on_curso_pushButton_clicked();
    void on_par_pushButton_clicked();

    void on_consultar_pushButton_clicked();

    void on_back_clicked();

private:
    Ui::GradesWidget *ui;
    QJsonObject jsonObjData;
    QJsonObject jsonObjGradeSeleted;
    QString selectedGrade,selectedPar;
    QString selectedGradeId;
    QList <QPushButton*> cursos_pushButton_list;
    QList <QPushButton*> par_pushButton_list;
};

#endif // GRADESWIDGET_H
