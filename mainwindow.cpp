#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>

int min(int a,int b){
    if(a < b) return a;
    else return b;
}

int max(int a,int b){
    if(a > b) return a;
    else return b;
}

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    isOverlayShown = false;
    isInfoShown = false;
    isAlertShown = false;
    isPinShown = false;


    ui->setupUi(this);

    ui->centralWidgetMain->resize(QDesktopWidget().width(),QDesktopWidget().height());

    loginWidget = new LoginWidget(ui->centralWidgetMain);
    gradesWigdet = new GradesWidget(ui->centralWidgetMain);
    classWidget = new ClassWidget(ui->centralWidgetMain);
    alertWidget = new AlertWidget(this);
    infoWidget = new InfoWidget(this);
    pinWidget = new PINWidget(this);
    pinBlockWidget = new PINWidget(this);
    subjectWidget = new SubjectWidget(this);

    hideInfo();
    hideAlert();
    hidePIN();
    hidePINBlock();
    hideSubject();

    connect(loginWidget,SIGNAL(showPIN()),this,SLOT(showPIN()));
    connect(loginWidget,SIGNAL(showPIN()),this,SLOT(showOverlay()));
    connect(pinWidget,SIGNAL(sendClose()),this,SLOT(hidePIN()));
    connect(pinWidget,SIGNAL(sendClose()),this,SLOT(hideOverlay()));
    connect(pinWidget,SIGNAL(sendPin(QString)),loginWidget,SLOT(setLogin(QString)));
    connect(pinBlockWidget,SIGNAL(sendClose()),this,SLOT(logout()));
    connect(pinBlockWidget,SIGNAL(sendPin(QString)),this,SLOT(logback(QString)));
    connect(subjectWidget,SIGNAL(sendClose()),this,SLOT(hideSubject()));
    connect(subjectWidget,SIGNAL(sendClose()),this,SLOT(hideOverlay()));
    connect(subjectWidget,SIGNAL(sendSubject(QString)),this,SLOT(sendSubject(QString)));

    loginWidget->hide();
    gradesWigdet->hide();
    classWidget->hide();

    connect(loginWidget,SIGNAL(sendLogin(QString,QString,QString,QString)),this,SLOT(login(QString,QString,QString,QString)));
    connect(gradesWigdet,SIGNAL(sendGrade(QString,QString)),this,SLOT(grade(QString,QString)));

    connect(gradesWigdet,SIGNAL(sendClose()),this,SLOT(closeGrades()));
    connect(classWidget,SIGNAL(sendClose()),this,SLOT(closeClass()));
    connect(classWidget,SIGNAL(confirmClass(QJsonArray)),this,SLOT(confirmClass(QJsonArray)));
    connect(classWidget,SIGNAL(asist()),this,SLOT(asist()));
    connect(classWidget,SIGNAL(block()),this,SLOT(showPINBlock()));
    connect(classWidget,SIGNAL(block()),this,SLOT(showOverlay()));
    connect(infoWidget,SIGNAL(sendClose()),this,SLOT(confirmInfo()));
    connect(infoWidget,SIGNAL(sendClose()),this,SLOT(hideInfo()));
    connect(infoWidget,SIGNAL(sendClose()),this,SLOT(hideOverlay()));
    connect(alertWidget,SIGNAL(sendClose()),this,SLOT(hideAlert()));
    connect(alertWidget,SIGNAL(sendClose()),this,SLOT(hideOverlay()));
    connect(alertWidget,SIGNAL(sendAlert(QString)),this,SLOT(sendAsist(QString)));

    socket = new QTcpSocket();
    timerInit = new QTimer();
    connect(timerInit,SIGNAL(timeout()),this,SLOT(timerInitTimeout()));
    connect(socket,SIGNAL(readyRead()),this,SLOT(readyRead()));
    connect(socket,SIGNAL(connected()),this,SLOT(connected()));
    connect(socket,SIGNAL(disconnected()),this,SLOT(disconnected()));

    loadingGif = new QMovie();
    loadingGif->setFileName(":/res/loading.gif");
    ui->loadingGifLabel->setMovie(loadingGif);
    ui->loadingGifLabel->setGeometry(QDesktopWidget().width()/2 - 100,QDesktopWidget().height()/2 - 100,200,200);
    ui->refreshButton->resize(QDesktopWidget().size());

    receptionTimer = new QTimer();
    connect(receptionTimer,SIGNAL(timeout()),this,SLOT(receptionTimerTimeout()));

    userId = "not logged";

    tryToConnect();
}

MainWindow::~MainWindow()
{
    delete ui;
}



void MainWindow::connected(){

    QJsonObject jsonObjMain,jsonObjData;
    QString mac;

#ifdef Q_OS_WIN
    mac = QNetworkInterface::interfaceFromName("wireless_0").hardwareAddress();
#else //Q_OS_ANDROID
    mac = QNetworkInterface::interfaceFromName("wlan0").hardwareAddress();
#endif

    jsonObjData["deviceMac"] = mac;

    jsonObjMain["type"] = "sc_init";
    jsonObjMain["data"] = jsonObjData;

    QJsonDocument jsonDoc(jsonObjMain);
    socket->write(jsonDoc.toJson(QJsonDocument::Compact));

}

void MainWindow::tryToConnect(){

    QHostAddress ip;
    QNetworkInterface ni;
    QString ipString,labelText;
    unsigned int puerto;
    loadingGif->start();
    ui->loadingGifLabel->show();
    ui->refreshButton->hide();

#ifdef Q_OS_WIN
     ni = QNetworkInterface::interfaceFromName("wireless_0");
     puerto = 5710;
#else //Q_OS_ANDROID
    ni = QNetworkInterface::interfaceFromName("wlan0");
    puerto = 5740;
#endif

    for(int i = 0; i < ni.addressEntries().size(); i++){
        ip = ni.addressEntries()[i].ip();
        if(ip.protocol() == QAbstractSocket::IPv4Protocol) break;
    }

    ipString =  ip.toString().split('.')[0] + "." +
                ip.toString().split('.')[1] + "." +
                ip.toString().split('.')[2] + "." +
                "50";                                   //   <---- CAMBIAR A DISCRECIÓN
                //ip.toString().split('.')[3];


    //socket->connectToHost(QHostAddress::LocalHost,5710);
    socket->connectToHost(QHostAddress(ipString),puerto);

    ui->label_resolution->setText(QString::number(height()) + "x" + QString::number(width()));
    ui->label_local->setText("Local: "+ ni.hardwareAddress() + ", " + ip.toString());
    ui->label_remoto->setText("Host: " + ipString + ":"  + QString::number(puerto));

    timerInit->start(6000);
}

void MainWindow::timerInitTimeout(){
    socket->blockSignals(true);
    socket->abort();
    socket->blockSignals(false);
    ui->refreshButton->show();
    ui->loadingGifLabel->hide();
    loadingGif->stop();
}

void MainWindow::disconnected(){
    userId = "not logged";
    loginWidget->hide();
    gradesWigdet->hide();
    classWidget->hide();
    tryToConnect();
}

void MainWindow::receptionTimerTimeout(){
    receptionBuffer.clear();
}

void MainWindow::readyRead(){
    receptionTimer->start(500);
    QByteArray qba = socket->readAll();

    receptionBuffer.append(qba);

    QJsonObject jsonObjData,jsonObjMain = QJsonDocument::fromJson(receptionBuffer).object();

    QString jsonType = jsonObjMain["type"].toString();
    if(jsonType == "sc_appInit"){
        timerInit->stop();
        loadingGif->stop();
        jsonObjData = jsonObjMain["data"].toObject();
        deviceId = jsonObjData["deviceId"].toString();
        loginWidget->populate(jsonObjData);
        gradesWigdet->populate(jsonObjData);
        alertWidget->populate(jsonObjData);

        loginWidget->show();
        gradesWigdet->hide();
        classWidget->hide();

        currentScreen = loginWidget;

    }

    if(jsonType == "sc_info"){
        jsonObjMain = jsonObjMain["data"].toObject();
        infoWidget->populate(jsonObjMain);
        messageId = jsonObjMain["messageId"].toString();
        showInfo();
        showOverlay();
        return;
    }

    if(jsonType == "sc_login"){
        gradesWigdet->setTeacherLabel("Ingresado como " + userName);
        gradesWigdet->show();
        loginWidget->hide();
        classWidget->hide();

        currentScreen = gradesWigdet;
        return;
    }

    if(jsonType == "sc_subjectList"){

        jsonObjData = jsonObjMain["data"].toObject();
        subjectWidget->populate(jsonObjData);

        showSubject();
        showOverlay();
    }

    if(jsonType == "sc_studentsList"){
        jsonObjMain = jsonObjMain["data"].toObject();
        classWidget->populate(jsonObjMain);
        classWidget->show();
        loginWidget->hide();
        gradesWigdet->hide();

        currentScreen = classWidget;
        return;
    }

    if(jsonType == "sc_updateStudent"){
        jsonObjMain = jsonObjMain["data"].toObject();
        classWidget->updateStudent(jsonObjMain);
        classWidget->show();
        loginWidget->hide();
        gradesWigdet->hide();

        currentScreen = classWidget;
        return;
    }

}

void MainWindow::login(QString userName,QString userId,QString roleId,QString pin){

    QJsonObject jsonObjMain,jsonObjData;
    this->userId = userId;
    this->roleId = roleId;
    this->pin = pin;

    hidePIN();
    hideOverlay();

    jsonObjData["pin"] = pin;
    jsonObjData["roleId"] = roleId;
    jsonObjData["deviceId"] = deviceId;
    jsonObjData["userId"] = userId;

    jsonObjMain["data"] = jsonObjData;
    jsonObjMain["type"] = "sc_userLogin";

    QJsonDocument jsonDoc(jsonObjMain);
    socket->write(jsonDoc.toJson(QJsonDocument::Compact));

    this->userName = userName;
}



void MainWindow::grade(QString gradeId,QString className){
    QJsonObject jsonObjMain,jsonObjData;
    this->gradeId = gradeId;

    jsonObjData["deviceId"] = deviceId;
    jsonObjData["userId"] = userId;
    jsonObjData["gradeId"] = gradeId;
    jsonObjData["subjectId"] = subjectId;

    jsonObjMain["data"] = jsonObjData;
    jsonObjMain["type"] = "sc_selectGrade";


    QJsonDocument jsonDoc(jsonObjMain);
    socket->write(jsonDoc.toJson(QJsonDocument::Compact));

    classWidget->setTeacherLabel(className + " - " + userName);

}

void MainWindow::closeClass(){
    gradesWigdet->show();
    loginWidget->hide();
    classWidget->hide();
}

void MainWindow::closeGrades(){
    loginWidget->show();
    gradesWigdet->hide();
    classWidget->hide();
}


void MainWindow::on_refreshButton_clicked()
{
    tryToConnect();
}

void MainWindow::updateStudentStauts(QString studentId,bool isPresent,QString text,QString id){

    QJsonObject jsonObjMain,jsonObjData;

    jsonObjData["deviceId"] = deviceId;
    jsonObjData["userId"] = userId;
    jsonObjData["gradeId"] = gradeId;
    jsonObjData["studentId"] = studentId;
    jsonObjData["isPresent"] = !isPresent;
    jsonObjData["reasonText"] = text;
    jsonObjData["reasonId"] = id;

    jsonObjMain["data"] = jsonObjData;
    jsonObjMain["type"] = "sc_updateStudent";

    QJsonDocument jsonDoc(jsonObjMain);
    socket->write(jsonDoc.toJson(QJsonDocument::Compact));

}

void MainWindow::confirmClass(QJsonArray array){
    QJsonObject jsonObjMain,jsonObjData;

    jsonObjData["deviceId"] = deviceId;
    jsonObjData["userId"] = userId;
    jsonObjData["roleId"] = roleId;
    jsonObjData["gradeId"] = gradeId;
    jsonObjData["subjectId"] = subjectId;
    jsonObjData["presenceList"] = array;

    jsonObjMain["data"] = jsonObjData;
    jsonObjMain["type"] = "sc_confirmPresence";

    QJsonDocument jsonDoc(jsonObjMain);
    socket->write(jsonDoc.toJson(QJsonDocument::Compact));
}

void MainWindow::sendSubject(QString subjectId){
    QJsonObject jsonObjMain,jsonObjData;
    hideSubject();
    hideOverlay();

    this->subjectId = subjectId;

    jsonObjData["deviceId"] = deviceId;
    jsonObjData["userId"] = userId;
    jsonObjData["roleId"] = gradeId;
    jsonObjData["gradeId"] = gradeId;
    jsonObjData["subjectId"] = subjectId;

    jsonObjMain["data"] = jsonObjData;
    jsonObjMain["type"] = "sc_selectSubject";

    QJsonDocument jsonDoc(jsonObjMain);
    socket->write(jsonDoc.toJson(QJsonDocument::Compact));
}

void MainWindow::asist(){
    showAlert();
    showOverlay();
}

void MainWindow::sendAsist(QString id){
    QJsonObject jsonObjMain,jsonObjData;
    hideAlert();
    hideOverlay();

    this->subjectId = subjectId;

    jsonObjData["deviceId"] = deviceId;
    jsonObjData["userId"] = userId;
    jsonObjData["roleId"] = gradeId;
    jsonObjData["gradeId"] = gradeId;
    jsonObjData["subjectId"] = subjectId;
    jsonObjData["alertId"] = id;

    jsonObjMain["data"] = jsonObjData;
    jsonObjMain["type"] = "sc_classroomAlert";

    QJsonDocument jsonDoc(jsonObjMain);
    socket->write(jsonDoc.toJson(QJsonDocument::Compact));
}


void MainWindow::showOverlay(){
    isOverlayShown = true;
    ui->overlay->raise();
    ui->overlay->move(0,0);
}

void MainWindow::hideOverlay(){
    isOverlayShown = false;
    ui->overlay->lower();
    ui->overlay->move(width(),0);
}

void MainWindow::showInfo(){
    isInfoShown = true;
    int x = (width() - infoWidget->width())*0.5;
    int y = (height() - infoWidget->height())*0.5;
    infoWidget->move(x,y);
}

void MainWindow::showAlert(){
    isAlertShown = true;
    int x = (width() - alertWidget->width())*0.5;
    int y = (height() - alertWidget->height())*0.5;
    alertWidget->move(x,y);
}

void MainWindow::showPIN(){
    isPinShown = true;
    int x = (width() - pinWidget->width())*0.5;
    int y = (height() - pinWidget->height())*0.5;
    pinWidget->move(x,y);
}

void MainWindow::showPINBlock(){
    isPinBlockShown = true;
    int x = (width() - pinBlockWidget->width())*0.5;
    int y = (height() - pinBlockWidget->height())*0.5;
    pinBlockWidget->move(x,y);
}

void MainWindow::showSubject(){
    isSubjectShown = true;
    int x = (width() - subjectWidget->width())*0.5;
    int y = (height() - subjectWidget->height())*0.5;
    subjectWidget->move(x,y);
}

void MainWindow::confirmInfo(){
    QJsonObject jsonObjMain,jsonObjData;


    jsonObjData["deviceId"] = deviceId;
    jsonObjData["userId"] = userId;
    jsonObjData["messageId"] = messageId;

    jsonObjMain["data"] = jsonObjData;
    jsonObjMain["type"] = "sc_info_check";

    QJsonDocument jsonDoc(jsonObjMain);
    socket->write(jsonDoc.toJson(QJsonDocument::Compact));



}

void MainWindow::hideInfo(){
    isInfoShown = false;
    infoWidget->move(width(),height());
}

void MainWindow::hideAlert(){
    isAlertShown = false;
    alertWidget->move(width(),height());
}


void MainWindow::hidePIN(){
    isPinShown = false;
    pinWidget->move(width(),height());
}

void MainWindow::hidePINBlock(){
    isPinBlockShown = false;
    pinBlockWidget->move(width(),height());
}

void MainWindow::hideSubject(){
    isSubjectShown = false;
    subjectWidget->move(width(),height());
}


void MainWindow::logout(){
    hidePINBlock();
    hideOverlay();
    loginWidget->show();
    gradesWigdet->hide();
    classWidget->hide();
}

void MainWindow::logback(QString pinT){
    if(pinT == pin){
        hidePINBlock();
        hideOverlay();
    }
}

void MainWindow::resizeEvent(QResizeEvent*){

    //QSize desktopSize = QDesktopWidget().size();
    QSize desktopSize = QSize(1024,600);
    resize(desktopSize);
    ui->label_resolution->setText(QString::number(height()) + "x" + QString::number(width()));

    ui->centralWidgetMain->resize(desktopSize);
    ui->refreshButton->resize(desktopSize);
    ui->overlay->resize(desktopSize);
     ui->loadingGifLabel->move(width()/2 - 100, height()/2 - 100);
    if(isInfoShown) showInfo();
    else hideInfo();
    if(isAlertShown) showAlert();
    else hideAlert();
    if(isPinShown) showPIN();
    else hidePIN();
    if(isOverlayShown) showOverlay();
    else hideOverlay();
    loginWidget->updateSize(desktopSize);
    gradesWigdet->updateSize(desktopSize);
    classWidget->updateSize(desktopSize);
}

