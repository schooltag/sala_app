#include "classwidget.h"
#include "ui_classwidget.h"

ClassWidget::ClassWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ClassWidget)
{
    ui->setupUi(this);


    int width = QDesktopWidget().width();
    int height = QDesktopWidget().height();

    resize(width,height);
    ui->frame->resize(width,height);

    student_pushbutton_list.append(ui->pushButton_student_1);
    student_pushbutton_list.append(ui->pushButton_student_2);
    student_pushbutton_list.append(ui->pushButton_student_3);
    student_pushbutton_list.append(ui->pushButton_student_4);
    student_pushbutton_list.append(ui->pushButton_student_5);
    student_pushbutton_list.append(ui->pushButton_student_6);
    student_pushbutton_list.append(ui->pushButton_student_7);
    student_pushbutton_list.append(ui->pushButton_student_8);
    student_pushbutton_list.append(ui->pushButton_student_9);
    student_pushbutton_list.append(ui->pushButton_student_10);
    student_pushbutton_list.append(ui->pushButton_student_11);
    student_pushbutton_list.append(ui->pushButton_student_12);
    student_pushbutton_list.append(ui->pushButton_student_13);
    student_pushbutton_list.append(ui->pushButton_student_14);
    student_pushbutton_list.append(ui->pushButton_student_15);
    student_pushbutton_list.append(ui->pushButton_student_16);
    student_pushbutton_list.append(ui->pushButton_student_17);
    student_pushbutton_list.append(ui->pushButton_student_18);
    student_pushbutton_list.append(ui->pushButton_student_19);
    student_pushbutton_list.append(ui->pushButton_student_20);
    student_pushbutton_list.append(ui->pushButton_student_21);
    student_pushbutton_list.append(ui->pushButton_student_22);
    student_pushbutton_list.append(ui->pushButton_student_23);
    student_pushbutton_list.append(ui->pushButton_student_24);
    student_pushbutton_list.append(ui->pushButton_student_25);
    student_pushbutton_list.append(ui->pushButton_student_26);
    student_pushbutton_list.append(ui->pushButton_student_27);
    student_pushbutton_list.append(ui->pushButton_student_28);
    student_pushbutton_list.append(ui->pushButton_student_29);
    student_pushbutton_list.append(ui->pushButton_student_30);
    student_pushbutton_list.append(ui->pushButton_student_31);
    student_pushbutton_list.append(ui->pushButton_student_32);
    student_pushbutton_list.append(ui->pushButton_student_33);
    student_pushbutton_list.append(ui->pushButton_student_34);
    student_pushbutton_list.append(ui->pushButton_student_35);
    student_pushbutton_list.append(ui->pushButton_student_36);
    student_pushbutton_list.append(ui->pushButton_student_37);
    student_pushbutton_list.append(ui->pushButton_student_38);
    student_pushbutton_list.append(ui->pushButton_student_39);
    student_pushbutton_list.append(ui->pushButton_student_40);
    student_pushbutton_list.append(ui->pushButton_student_41);
    student_pushbutton_list.append(ui->pushButton_student_42);
    student_pushbutton_list.append(ui->pushButton_student_43);
    student_pushbutton_list.append(ui->pushButton_student_44);
    student_pushbutton_list.append(ui->pushButton_student_45);

    timer = new QTimer();
    connect(timer,SIGNAL(timeout()),this,SLOT(timeout()));


    for(int i = 0; i < student_pushbutton_list.size(); i++){
        student_pushbutton_list[i]->setMaximumWidth(width/3);
        connect(student_pushbutton_list[i],SIGNAL(clicked(bool)),this,SLOT(student_pushbutton_pressed()));
    }
}

ClassWidget::~ClassWidget()
{
    delete ui;
}

void ClassWidget::populate(QJsonObject jsonObjData){
    QJsonArray jsonArray = jsonObjData["studentsList"].toArray();
    for(int i = 0; i < 45; i++){
        student_pushbutton_list[i]->setEnabled(false);
        student_pushbutton_list[i]->setChecked(false);
        student_pushbutton_list[i]->setProperty("edited",false);
        student_pushbutton_list[i]->setProperty("requested",false);
        student_pushbutton_list[i]->hide();
    }
    for(int i = 0; i < jsonArray.size() && i < 45; i++){
        student_pushbutton_list[i]->show();
        if(jsonArray[i].toObject()["requested"].toString() == "1"){
            student_pushbutton_list[i]->setProperty("requested",true);
            student_pushbutton_list[i]->setEnabled(false);
        }else{
            student_pushbutton_list[i]->setEnabled(true);
            student_pushbutton_list[i]->setProperty("requested",false);
            if(jsonArray[i].toObject()["present"].toString() == "1") student_pushbutton_list[i]->setChecked(true);
        }
        student_pushbutton_list[i]->setText(jsonArray[i].toObject()["name"].toString() + " " +jsonArray[i].toObject()["last_name"].toString());
        student_pushbutton_list[i]->setProperty("id",jsonArray[i].toObject()["id"].toString());
    }


    timer->start(1000);
    setStyleSheet(styleSheet());
}

void ClassWidget::setTeacherLabel(QString teacherLabel){
    ui->teacherLabel->setText(teacherLabel);
}

void ClassWidget::updateStudent(QJsonObject jsonObjData){
    QString id = jsonObjData["studentId"].toString();
    bool isPresent = (jsonObjData["present"].toString() == "1")? true: false;
    for(int i = 0; i < student_pushbutton_list.size(); i++){
        if(student_pushbutton_list[i]->property("id").toString() == id){
            if(jsonObjData["requested"].toString() == "true"){
                student_pushbutton_list[i]->setProperty("requested",true);
                student_pushbutton_list[i]->setEnabled(false);
            }else{
                student_pushbutton_list[i]->setEnabled(true);
                student_pushbutton_list[i]->setChecked(isPresent);
                student_pushbutton_list[i]->setProperty("edited",false);
            }

        }
    }
}

void ClassWidget::timeout(){
    timer->start(1000);

    for(int i = 0; i < 45; i++){
        if(student_pushbutton_list[i]->property("requested").toBool()){
            bool isChecked = student_pushbutton_list[i]->isChecked();
            student_pushbutton_list[i]->setChecked(!isChecked);
        }
    }
}


void ClassWidget::on_back_clicked()
{
    timer->stop();
    emit sendClose();
}

void ClassWidget::updateSize(QSize desktopSize){
    resize(desktopSize);
    ui->frame->resize(desktopSize);

}


void ClassWidget::on_confirmationButton_clicked()
{
    QJsonArray array;
    for(int i = 0; i < student_pushbutton_list.size(); i++){
        student_pushbutton_list[i]->setProperty("edited",false);
        if(student_pushbutton_list[i]->isEnabled()){
            QString id = student_pushbutton_list[i]->property("id").toString();
            QString isPresent = (student_pushbutton_list[i]->isChecked())? "1":"0";
            QJsonObject jsonObjTmp;
            jsonObjTmp["id"] = id;
            jsonObjTmp["present"] = isPresent;
            array.append(jsonObjTmp);
        }
    }
    setStyleSheet(styleSheet());
    emit confirmClass(array);
}

void ClassWidget::student_pushbutton_pressed(){
   QObject::sender()->setProperty("edited",true);
   setStyleSheet(styleSheet());
}

void ClassWidget::on_asistButton_clicked()
{
    emit asist();
}

void ClassWidget::on_blockButton_clicked()
{
    emit block();
}
