#include "pinwidget.h"
#include "ui_pinwidget.h"

PINWidget::PINWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::PINWidget)
{
    ui->setupUi(this);
}

PINWidget::~PINWidget()
{
    delete ui;
}

void PINWidget::on_pushButton_clicked()
{
    char num = '1';
    QString txt = ui->lineEdit->text();
    if(txt.size() < MAXCHAR) ui->lineEdit->setText(txt + num);
}

void PINWidget::on_pushButton_2_clicked()
{
    char num = '2';
    QString txt = ui->lineEdit->text();
    if(txt.size() < MAXCHAR) ui->lineEdit->setText(txt + num);
}

void PINWidget::on_pushButton_3_clicked()
{
    char num = '3';
    QString txt = ui->lineEdit->text();
    if(txt.size() < MAXCHAR) ui->lineEdit->setText(txt + num);
}

void PINWidget::on_pushButton_4_clicked()
{
    char num = '4';
    QString txt = ui->lineEdit->text();
    if(txt.size() < MAXCHAR) ui->lineEdit->setText(txt + num);
}

void PINWidget::on_pushButton_5_clicked()
{
    char num = '5';
    QString txt = ui->lineEdit->text();
    if(txt.size() < MAXCHAR) ui->lineEdit->setText(txt + num);
}

void PINWidget::on_pushButton_6_clicked()
{
    char num = '6';
    QString txt = ui->lineEdit->text();
    if(txt.size() < MAXCHAR) ui->lineEdit->setText(txt + num);
}

void PINWidget::on_pushButton_7_clicked()
{
    char num = '7';
    QString txt = ui->lineEdit->text();
    if(txt.size() < MAXCHAR) ui->lineEdit->setText(txt + num);
}

void PINWidget::on_pushButton_8_clicked()
{
    char num = '8';
    QString txt = ui->lineEdit->text();
    if(txt.size() < MAXCHAR) ui->lineEdit->setText(txt + num);
}

void PINWidget::on_pushButton_9_clicked()
{
    char num = '9';
    QString txt = ui->lineEdit->text();
    if(txt.size() < MAXCHAR) ui->lineEdit->setText(txt + num);
}

void PINWidget::on_pushButton_10_clicked()
{
    char num = '0';
    QString txt = ui->lineEdit->text();
    if(txt.size() < MAXCHAR) ui->lineEdit->setText(txt + num);
}

void PINWidget::on_pushButton_del_clicked()
{
    QString txt = ui->lineEdit->text();
    if(txt.size() == 0) return;
    txt.chop(1);
    ui->lineEdit->setText(txt);
}

void PINWidget::clearPIN(){
   ui->lineEdit->clear();
}

void PINWidget::on_pushButton_12_clicked()
{
    emit sendClose();
    ui->lineEdit->clear();
}

void PINWidget::on_pushButton_11_clicked()
{
    if(ui->lineEdit->text().size() > 4) return;
    emit sendPin(ui->lineEdit->text());
    ui->lineEdit->clear();
}
