#include "reasonswidget.h"
#include "ui_reasonswidget.h"

ReasonsWidget::ReasonsWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ReasonsWidget)
{
    ui->setupUi(this);
}

ReasonsWidget::~ReasonsWidget()
{
    delete ui;
}

void ReasonsWidget::populate(QJsonArray reasonArray){
    this->reasonArray = reasonArray;
}

void ReasonsWidget::setIsPresent(bool isPresent){
    clearButtons();
    for(int i = 0; i < reasonArray.size(); i++){
        QJsonObject jobj = reasonArray.at(i).toObject();
        if(isPresent){
            if(jobj["isPresent"].toString() == "true"){
                addButton(jobj["text"].toString(),jobj["id"].toString());
            }
        }else{
            if(jobj["isPresent"].toString() == "false"){
                addButton(jobj["text"].toString(),jobj["id"].toString());
            }
        }
    }
}

void ReasonsWidget::addButton(QString text, QString id){
    QPushButton* button = new QPushButton();
    connect(button,SIGNAL(clicked(bool)),this,SLOT(reasonsButtonClicked()));
    button->setText(text);

    button->setProperty("id",id);

    button->setMinimumHeight(65);
    button->setMaximumHeight(65);
    button->setMinimumWidth(600);
    button->setMaximumWidth(600);
    button->setSizePolicy(QSizePolicy::Minimum,QSizePolicy::Maximum);
    button->setFocusPolicy(Qt::NoFocus);
    ui->buttonArea->layout()->addWidget(button);

    unsigned int expectedHeight = 105 + ui->buttonArea->layout()->count()*65;

    if(expectedHeight > 350)
        resize(width(),expectedHeight);
}

void ReasonsWidget::clearButtons(){
    QLayoutItem *child;
    while ((child = ui->buttonArea->layout()->takeAt(0)) != 0) {
       delete child->widget();
    }
    resize(width(),350);
}

void ReasonsWidget::populate(QJsonObject dataObj){
    clearButtons();
    reasonArray = dataObj["reasonList"].toArray();
}

void ReasonsWidget::reasonsButtonClicked(){

}

void ReasonsWidget::on_pushButton_clicked()
{
    emit sendClose();
}
