#ifndef REASONSWIDGET_H
#define REASONSWIDGET_H

#include <QWidget>
#include <QJsonArray>
#include <QJsonObject>

namespace Ui {
class ReasonsWidget;
}

class ReasonsWidget : public QWidget
{
    Q_OBJECT

public:
    explicit ReasonsWidget(QWidget *parent = 0);
    ~ReasonsWidget();
    void addButton(QString text,QString id);
    void clearButtons();
    void populate(QJsonArray reasonArray);
    void setIsPresent(bool isPresent);
    void populate(QJsonObject dataObj);

private slots:
    void reasonsButtonClicked();
    void on_pushButton_clicked();

private:
    Ui::ReasonsWidget *ui;
    QJsonArray reasonArray;

signals:
    void sendClose();

};

#endif // REASONSWIDGET_H
